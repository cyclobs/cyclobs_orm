# cyclobs_orm

SQLAlchemy ORM for cyclobs that eases database interactions

Usage example
```
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from cyclobs_orm import SimpleTrack

engine = create_engine(database_url)
Session = sessionmaker(bind=engine)
sess = Session()
q = sess.query(SimpleTrack).all()
```
