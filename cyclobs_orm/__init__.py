from geoalchemy2 import Geography
from sqlalchemy.orm import relationship
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import (
    Column,
    Integer,
    String,
    ForeignKey,
    Table,
    cast,
    TIMESTAMP,
    Float,
    Boolean,
    Interval,
    ForeignKeyConstraint,
)
from geoalchemy2.shape import to_shape
from sqlalchemy.types import TypeDecorator
from geoalchemy2 import Geography, Geometry


class GeographyToGeometry(TypeDecorator):
    impl = Geography

    def column_expression(self, column):
        return cast(column, Geometry)


Base = declarative_base()

# Link table for link table between Basin and Acquisition
basin_acquisition = Table(
    "Basin_Acquisition",
    Base.metadata,
    Column("name_Basin", String(), ForeignKey("Basin.name"), primary_key=True),
    Column("id_Acquisition", String(), ForeignKey("Acquisition.id"), primary_key=True),
)

# Link table for self relationship for table Basin (parent/child relationships)
basin_basin = Table(
    "Basin_Basin",
    Base.metadata,
    Column("name_parent", String(), ForeignKey("Basin.name"), primary_key=True),
    Column("name_child", String(), ForeignKey("Basin.name"), primary_key=True),
)


# Link table between Basin and Track table
# basin_track = Table('Basin_Track',
#                    Base.metadata,
#                    Column('name_Basin', String(), ForeignKey('Basin.name'), primary_key=True),
#                    Column('sid_Track', String(), ForeignKey('Track.sid'), primary_key=True),
#                    Column('source_Track', String(), ForeignKey('Track.source'), primary_key=True),
#                    Column('file_Track', String(), ForeignKey('Track.file'), primary key=True),
#                    )


class Basin_Track(Base):
    __tablename__ = "Basin_Track"

    name_Basin = Column(String(), ForeignKey("Basin.name"), primary_key=True)
    sid_Track = Column(String(), primary_key=True)
    source_Track = Column(String(), primary_key=True)
    file_Track = Column(String(), primary_key=True)

    __table_args__ = (
        ForeignKeyConstraint(
            ["sid_Track", "file_Track", "source_Track"],
            ["Track.sid", "Track.file", "Track.source"],
        ),
        {},
    )


# basin_simpletrack = Table('many_Basin_has_many_SimpleTrack',
#                    Base.metadata,
#                    Column('name_Basin', String(), ForeignKey('Basin.name'), primary key=True),
#                    Column('id_SimpleTrack', String(), ForeignKey('SimpleTrack.id'), primary key=True),
#                    Column('date_SimpleTrack', String(), ForeignKey('SimpleTrack.date'), primary key=True),
#                    )

# acq_acq = Table('many_Acquisition_has_many_Acquisition', Base.metadata,
#                 Column('id_Acquisition', String(), ForeignKey('Acquisition.id'), primary_key=True),
#                 Column('id_Acquisition1', String(), ForeignKey('Acquisition.id'), primary key=True),
#                 Column('area_intersect', Integer()),
#                 Column('coloc_product_path', String())
#                 )


class Product(Base):
    __tablename__ = "Product"

    id = Column(String(), primary_key=True, nullable=False)
    filename = Column(String(), unique=True, nullable=False)
    grid_type = Column(String(), nullable=False)
    id_Acquisition = Column(
        String(), ForeignKey("Acquisition.id", ondelete="CASCADE"), nullable=False
    )
    acquisition = relationship(
        "Acquisition", back_populates="products", passive_deletes=True
    )


class Basin_SimpleTrack(Base):
    __tablename__ = "many_Basin_has_many_SimpleTrack"
    name_Basin = Column(String(), ForeignKey("Basin.name"), primary_key=True)
    sid_SimpleTrack = Column(String(), primary_key=True)
    date_SimpleTrack = Column(TIMESTAMP, primary_key=True)
    file_SimpleTrack = Column(String(), primary_key=True)
    source_SimpleTrack = Column(String(), primary_key=True)

    __table_args__ = (
        ForeignKeyConstraint(
            [
                "date_SimpleTrack",
                "sid_SimpleTrack",
                "file_SimpleTrack",
                "source_SimpleTrack",
            ],
            [
                "SimpleTrack.date",
                "SimpleTrack.sid",
                "SimpleTrack.file",
                "SimpleTrack.source",
            ],
        ),
        {},
    )

    # acquisitions = relationship("Acquisition", back_populates="simpletrack_assoc")
    # simpletracks = relationship("SimpleTrack", back_populates="acquisition_assoc")


class Acquisition_SimpleTrack(Base):
    __tablename__ = "many_Acquisition_has_many_SimpleTrack"
    id_Acquisition = Column(String(), ForeignKey("Acquisition.id"), primary_key=True)
    sid_SimpleTrack = Column(String(), primary_key=True)
    date_SimpleTrack = Column(TIMESTAMP, primary_key=True)
    file_SimpleTrack = Column(String(), primary_key=True)
    source_SimpleTrack = Column(String(), primary_key=True)
    eye_distance = Column(Float())
    eye_contained = Column(Boolean())
    time_diff = Column(Interval())
    coloc_file = Column(String())

    __table_args__ = (
        ForeignKeyConstraint(
            [
                "date_SimpleTrack",
                "sid_SimpleTrack",
                "file_SimpleTrack",
                "source_SimpleTrack",
            ],
            [
                "SimpleTrack.date",
                "SimpleTrack.sid",
                "SimpleTrack.file",
                "SimpleTrack.source",
            ],
        ),
        {},
    )

    acquisitions = relationship(
        "Acquisition", back_populates="simpletrack_assoc", overlaps="simpletrack"
    )
    simpletracks = relationship("SimpleTrack", back_populates="acquisition_assoc")


class Acquisition_Acquisition(Base):
    __tablename__ = "many_Acquisition_has_many_Acquisition"
    id_Acquisition = Column(String(), ForeignKey("Acquisition.id"), primary_key=True)
    id_Acquisition1 = Column(String(), ForeignKey("Acquisition.id"), primary_key=True)
    area_intersect = Column(Integer())
    coloc_product_path = Column(String())


# Acquisition table
class Acquisition(Base):
    __tablename__ = "Acquisition"
    id = Column(String(), primary_key=True, unique=True, nullable=False)
    quicklook_path = Column(String(), unique=True)
    mission = Column(String(), nullable=False)
    mission_short = Column(String(), nullable=False)
    instrument = Column(String(), nullable=False)
    instrument_short = Column(String(), nullable=False)
    startdate = Column(TIMESTAMP, nullable=False)
    stopdate = Column(TIMESTAMP, nullable=False)

    geom = Column(GeographyToGeometry(geometry_type="POLYGON", srid=4326))
    # geom = Column(Geography('Polygon'), index=True, nullable=False)

    ##### OLD TO KEEP
    ## This is the way to create a relationship that is using geographical function request on the fly.
    # simpletrack = relationship("SimpleTrack",
    #                primaryjoin="and_(func.ST_Intersects(foreign(Acquisition.geom), SimpleTrack.geom).as_comparison(1, 2),"
    #                "func.abs(extract('epoch', SimpleTrack.date - foreign(Acquisition.startdate)) / 3600) < 1.5)",
    #                viewonly=True,
    #                backref="acquisitions")
    #####
    products = relationship(
        "Product", back_populates="acquisition", passive_deletes=True
    )

    # simpletrack = relationship("SimpleTrack", back_populates="acquisitions")

    simpletrack = relationship(
        "SimpleTrack",
        secondary="many_Acquisition_has_many_SimpleTrack",
        back_populates="acquisitions",
        overlaps="simpletracks"
    )
    simpletrack_assoc = relationship(
        "Acquisition_SimpleTrack", back_populates="acquisitions", overlaps="simpletrack"
    )

    # sfmr_track = relationship("Track", primaryjoin="and_(foreign(Acquisition.sid_SimpleTrack) == Track.sid,"
    #                                               "Track.source.like('sfmr%'),"
    #                                               "Acquisition.startdate >= Track.startdate,"
    #                                               "Acquisition.startdate <= Track.stopdate)",
    #                          backref="sfmr_acq")

    basins = relationship(
        "Basin", secondary=basin_acquisition, back_populates="acquisitions"
    )

    basin_geom = relationship(
        "Basin",
        primaryjoin="func.ST_Intersects(foreign(Acquisition.geom), Basin.geom).as_comparison(1, 2)",
        viewonly=True,
        backref="acquisitions_geom",
        sync_backref=False,
    )

    acq_coloc = relationship(
        "Acquisition",
        secondary="many_Acquisition_has_many_Acquisition",
        #  back_populates="acquisitions",
        primaryjoin=id == Acquisition_Acquisition.id_Acquisition,
        secondaryjoin=id == Acquisition_Acquisition.id_Acquisition1,
    )

    # acq_coloc_assoc = relationship("Acquisition_Acquisition", back_populates="acquisitions")

    saracq = relationship("SARAcq", uselist=False, back_populates="acquisition")

    smosacq = relationship("SMOSAcq", uselist=False, back_populates="acquisition")

    center_analysis = relationship("CenterAnalysis", back_populates="acquisition")

    def to_dict(self):
        data = {
            "id": self.id,
            "mission": self.mission,
            "instrument": self.instrument,
            "startdate": self.startdate,
            "stopdate": self.stopdate,
            "footprint": to_shape(self.geom).wkt,
        }
        return data


class CenterAnalysis(Base):
    __tablename__ = "Center_analysis"

    id_Acquisition = Column(
        String(), ForeignKey("Acquisition.id"), primary_key=True, nullable=False
    )
    vmax = Column(Float(), nullable=True)
    eye_center = Column(Geography(), nullable=False)
    rmw = Column(Float(), nullable=True)
    control_plot_path = Column(String())
    fix_path = Column(String(), nullable=False)
    storm_fix_path = Column(String(), nullable=False)
    analysis_product_path = Column(String(), nullable=False)
    center_quality_flag = Column(Float(), nullable=False)
    rad_34_neq = Column(Float(), nullable=True)
    rad_34_nwq = Column(Float(), nullable=True)
    rad_34_seq = Column(Float(), nullable=True)
    rad_34_swq = Column(Float(), nullable=True)
    rad_50_neq = Column(Float(), nullable=True)
    rad_50_nwq = Column(Float(), nullable=True)
    rad_50_seq = Column(Float(), nullable=True)
    rad_50_swq = Column(Float(), nullable=True)
    rad_64_neq = Column(Float(), nullable=True)
    rad_64_nwq = Column(Float(), nullable=True)
    rad_64_seq = Column(Float(), nullable=True)
    rad_64_swq = Column(Float(), nullable=True)

    rad_34_neq_quality = Column(Float(), nullable=True)
    rad_34_nwq_quality = Column(Float(), nullable=True)
    rad_34_seq_quality = Column(Float(), nullable=True)
    rad_34_swq_quality = Column(Float(), nullable=True)
    rad_50_neq_quality = Column(Float(), nullable=True)
    rad_50_nwq_quality = Column(Float(), nullable=True)
    rad_50_seq_quality = Column(Float(), nullable=True)
    rad_50_swq_quality = Column(Float(), nullable=True)
    rad_64_neq_quality = Column(Float(), nullable=True)
    rad_64_nwq_quality = Column(Float(), nullable=True)
    rad_64_seq_quality = Column(Float(), nullable=True)
    rad_64_swq_quality = Column(Float(), nullable=True)
    acquisition = relationship("Acquisition", back_populates="center_analysis")


# Basin table
class Basin(Base):
    __tablename__ = "Basin"
    name = Column(String(), primary_key=True, index=True, unique=True)
    fullname = Column(String(), index=True, unique=True)
    source = Column(String())
    geom = Column(Geography("Polygon"))

    children = relationship(
        "Basin",
        secondary=basin_basin,
        primaryjoin=name == basin_basin.c.name_parent,
        secondaryjoin=name == basin_basin.c.name_child,
    )

    parents = relationship(
        "Basin",
        secondary=basin_basin,
        primaryjoin=name == basin_basin.c.name_child,
        secondaryjoin=name == basin_basin.c.name_parent,
        overlaps="children"
    )

    acquisitions = relationship(
        "Acquisition", secondary=basin_acquisition, back_populates="basins"
    )

    tracks = relationship("Track", secondary="Basin_Track", back_populates="basins")

    simpletracks = relationship(
        "SimpleTrack",
        secondary="many_Basin_has_many_SimpleTrack",
        back_populates="basins",
    )

    def to_dict(self):
        dict = {"name": self.name, "fullname": self.fullname}
        return dict


# SARAcq table
class SARAcq(Base):
    __tablename__ = "SARAcq"

    id = Column(
        String(),
        ForeignKey("Acquisition.id"),
        primary_key=True,
        unique=True,
        nullable=False,
    )

    acquisition = relationship("Acquisition", back_populates="saracq", uselist=False)

    def to_dict(self):
        data = {}
        data["instrument"] = self.instrument
        return data


# SMOSAcq table
class SMOSAcq(Base):
    __tablename__ = "SMOSAcq"

    id = Column(
        String(),
        ForeignKey("Acquisition.id"),
        primary_key=True,
        unique=True,
        nullable=False,
    )
    SMOSSpecificCol = Column(Integer(), nullable=False)

    acquisition = relationship("Acquisition", back_populates="smosacq", uselist=False)


# Track table
class Track(Base):
    __tablename__ = "Track"
    sid = Column(String(), primary_key=True, index=True)
    source = Column(String(), primary_key=True, nullable=False)
    file = Column(String(), primary_key=True, nullable=False)
    name = Column(String(), index=True)
    startdate = Column(TIMESTAMP)
    stopdate = Column(TIMESTAMP)
    vmax = Column(Integer())
    geom = Column(Geography("Polygon"), index=True)

    simpletracks = relationship("SimpleTrack", back_populates="track")

    simpletracks_cyclone = relationship(
        "SimpleTrack",
        primaryjoin="and_(Track.sid==SimpleTrack.sid,"
        " Track.file==SimpleTrack.file,"
        " Track.source=='atcf')",
        backref="track_cyclone",
        overlaps="simpletracks"
    )

    # The two following relationship should give the same results, but one is using
    # precomputed table, the other computes the intersection on the fly in the request.
    basins_geom = relationship(
        "Basin",
        primaryjoin="func.ST_Intersects(foreign(Track.geom), Basin.geom).as_comparison(1, 2)",
        viewonly=True,
        backref="tracks_geom",
        sync_backref=False,
    )
    basins = relationship("Basin", secondary="Basin_Track", back_populates="tracks")

    def to_dict(self):
        dict = {
            "sid": self.sid,
            "name": self.name,
            "startdate": self.startdate,
            "stopdate": self.stopdate,
            "geom": to_shape(self.geom).wkt,
        }
        return dict


# SimpleTrack table
class SimpleTrack(Base):
    __tablename__ = "SimpleTrack"

    sid = Column(String(), primary_key=True, index=True)
    source = Column(String(), primary_key=True, index=True)
    file = Column(String(), primary_key=True, index=True)
    date = Column(TIMESTAMP, primary_key=True, index=True)
    vmax = Column(Float())
    geom = Column(Geography(), index=True)
    rad_34_neq = Column(Float())
    rad_34_nwq = Column(Float())
    rad_34_seq = Column(Float())
    rad_34_swq = Column(Float())
    rad_50_neq = Column(Float())
    rad_50_nwq = Column(Float())
    rad_50_seq = Column(Float())
    rad_50_swq = Column(Float())
    rad_64_neq = Column(Float())
    rad_64_nwq = Column(Float())
    rad_64_seq = Column(Float())
    rad_64_swq = Column(Float())
    rmw = Column(Float())
    type = Column(String())

    __table_args__ = (
        ForeignKeyConstraint(
            [sid, file, source], [Track.sid, Track.file, Track.source]
        ),
        {},
    )

    track = relationship("Track", back_populates="simpletracks")

    acquisitions = relationship(
        "Acquisition",
        secondary="many_Acquisition_has_many_SimpleTrack",
        back_populates="simpletrack",
        overlaps="acquisition_assoc"
    )
    acquisition_assoc = relationship(
        "Acquisition_SimpleTrack", back_populates="simpletracks", overlaps="acquisitions"
    )

    ibtracs = relationship("ibtracs", uselist=False, back_populates="simpletrack")
    atcf = relationship("atcf", uselist=False, back_populates="simpletrack")
    basins = relationship(
        "Basin",
        secondary="many_Basin_has_many_SimpleTrack",
        back_populates="simpletracks",
    )


# ibtracs table
class ibtracs(Base):
    __tablename__ = "ibtracs"

    sid = Column(String(), primary_key=True, unique=True, nullable=False)
    date = Column(String(), primary_key=True, unique=True, nullable=False)
    file = Column(String(), nullable=False)
    source = Column(String(), nullable=False)
    __table_args__ = (
        ForeignKeyConstraint(
            [date, sid, file, source],
            [SimpleTrack.date, SimpleTrack.sid, SimpleTrack.file, SimpleTrack.source],
        ),
        {},
    )

    NAME = Column(String(), index=True)
    BASIN = Column(String())
    VMAX = Column(Float())
    geom = Column(Geography("Point"), index=True)
    year = Column(Integer)

    USA_RMW = Column(Float())

    simpletrack = relationship("SimpleTrack", uselist=False, back_populates="ibtracs")

    def to_dict(self):
        pass


# atcf table
class atcf(Base):
    __tablename__ = "atcf"

    sid = Column(String(), primary_key=True, unique=True, nullable=False)
    date = Column(String(), primary_key=True, unique=True, nullable=False)
    source = Column(String(), nullable=False)
    file = Column(String(), nullable=False)
    __table_args__ = (
        ForeignKeyConstraint(
            [date, sid, file, source],
            [SimpleTrack.date, SimpleTrack.sid, SimpleTrack.file, SimpleTrack.source],
        ),
        {},
    )

    NAME = Column(String(), index=True)
    BASIN = Column(String())
    VMAX = Column(Float())
    RMW = Column(Float())
    RAD_34_NEQ = Column(Float())
    RAD_34_SEQ = Column(Float())
    RAD_34_SWQ = Column(Float())
    RAD_34_NWQ = Column(Float())
    RAD_50_NEQ = Column(Float())
    RAD_50_SEQ = Column(Float())
    RAD_50_SWQ = Column(Float())
    RAD_50_NWQ = Column(Float())
    RAD_64_NEQ = Column(Float())
    RAD_64_SEQ = Column(Float())
    RAD_64_SWQ = Column(Float())
    RAD_64_NWQ = Column(Float())
    geom = Column(Geography("Point"), index=True)

    simpletrack = relationship("SimpleTrack", uselist=False, back_populates="atcf")

    def to_dict(self):
        pass
