from setuptools import setup, find_packages
import os
import glob

install_requires = [
    'sqlalchemy<1.4',
    'geoalchemy2'
]

setup(name='cyclobs_orm',
      description='Cyclobs ORM to ease interaction with cyclobs database',
      url='https://gitlab.ifremer.fr/cyclobs/cyclobs',
      author = "Théo CEVAER",
      author_email = "theo.cevaer@ifremer.fr",
      license='GPL',
      packages=find_packages(),
      include_package_data=True,
      scripts=glob.glob('bin/**'),
      use_scm_version=True,
      setup_requires=['setuptools_scm'],
      #install_requires=install_requires,
      zip_safe = False
)